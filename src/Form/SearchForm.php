<?php

namespace Drupal\doi_search\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a DOI Publications form.
 */
class SearchForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'doi_search_search';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $doi = $this->getRequest()->query->get('doi');
    $form['search'] = [
      '#type' => 'textfield',
      '#title' => $this->t('DOI'),
      '#required' => TRUE,
      '#default_value' => $doi,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Search'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $doi = $form_state->getValues()['search'];
    $form_state->setRedirect('doi_search.page', [], ['query' => ['doi' => $doi]]);
  }

}
