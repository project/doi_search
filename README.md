## INTRODUCTION

 provides the ability translate nodes on creation and edition.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/doi_search

## FEATURES

This module provides a service (doi_search.manager) that allows retrieving information publications 
using DOI (https://www.doi.org/).

Usage example: $data = \Drupal::service('doi_search.manager')->getData({DOI})
It also provides a search page in /doi-search were you can search for the publications.

The API used can be consulted here http://api.crossref.org/swagger-ui

## INSTALLATION

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Maintainers

- Paulo Calado - [kallado](https://www.drupal.org/u/kallado)

This project has been sponsored by:
- Visit: [Javali](https://www.javali.pt) for more information