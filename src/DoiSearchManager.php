<?php

namespace Drupal\doi_search;

use Drupal\Core\Messenger\MessengerInterface;
use GuzzleHttp\ClientInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Doi Search Manager service used to retrieve doi data.
 */
class DoiSearchManager {
  use StringTranslationTrait;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Constructs a DoiSearchManager object.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The messenger.
   */
  public function __construct(MessengerInterface $messenger, ClientInterface $http_client) {
    $this->messenger = $messenger;
    $this->httpClient = $http_client;
  }

  /**
   * Gets the response form the CrossRef API given a doi.
   *
   * @param string $doi
   *   The DOI of the article you want to retrieve.
   *
   * @return object|void
   *   An object with the data if the request succeeds.
   */
  public function getData($doi) {
    try {
      $url = 'http://api.crossref.org/works/' . $doi;
      $request = $this->httpClient->request('GET', $url, ['http_errors' => FALSE]);
      $data = json_decode($request->getBody());
      return $data->message ?? [];
    }
    catch (\Exception $e) {
      $this->messenger->addError($this->t('Error retrieving Doi data'));
    }
  }

}
