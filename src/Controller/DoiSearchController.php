<?php

namespace Drupal\doi_search\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\doi_search\DoiSearchManager;

/**
 * Returns responses for DOI Publications routes.
 */
class DoiSearchController extends ControllerBase {

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $request;

  /**
   * The Doi search manager.
   *
   * @var Drupal\doi_search\DoiSearchManager
   */
  protected $doiSearchManager;

  /**
   * The controller constructor.
   *
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack service.
   * @param \Drupal\doi_search\DoiSearchManager $doi_search_manager
   *   The Doi search manager.
   */
  public function __construct(FormBuilderInterface $form_builder, RequestStack $request_stack, DoiSearchManager $doi_search_manager) {
    $this->formBuilder = $form_builder;
    $this->request = $request_stack;
    $this->doiSearchManager = $doi_search_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('form_builder'),
      $container->get('request_stack'),
      $container->get('doi_search.manager')
    );
  }

  /**
   * Builds the response.
   */
  public function build() {
    $form = $this->formBuilder->getForm('\Drupal\doi_search\Form\SearchForm');
    $doi = $this->request->getCurrentRequest()->query->get('doi');
    $info = [];
    if (!empty($doi)) {
      $data = $this->doiSearchManager->getData($doi);
      if (isset($data->DOI)) {
        $data->formattedAuthors = $this->formatAuthors($data->author);
        $data->pdf = $this->getPdfLink($data);
        $date = (array) $data->created;
        $data->date_text = date("d M Y", strtotime($date['date-time']));
        $data->date_time = $date['date-time'];
        $info = [
          "#theme" => 'doi_search_result',
          '#items' => (array) $data,
        ];
        $references = $this->getPublicationReferences($data);
      }
    }
    $build = [
      '#theme' => 'doi_search_page',
      '#items' => [
        'form' => $form,
        'result' => $info,
        'references' => $references ?? NULL,
        'empty' => empty($doi),
      ],
    ];
    return $build;
  }

  /**
   * It takes a DOI and returns an array of reference data.
   *
   * @param string $doi
   *   The DOI of the reference you want to retrieve.
   *
   * @return array|null
   *   An array of reference data or null if no data retrieved.
   */
  private function getReferenceData($doi) {
    if ($doi) {
      $data = (array) $this->doiSearchManager->getData($doi);
      if (!empty($data)) {
        $date = (array) $data['created'];
        $refData = [
          'title' => $this->getPublicationTitle($data),
          'author' => isset($data['author']) ? $this->formatAuthors($data['author']) : NULL,
          'year' => date("d M Y", strtotime($date['date-time'])),
          'doi' => $data['DOI'] ?? '',
          'url' => $data['URL'] ?? '',
        ];
      }
    }
    return $refData ?? NULL;
  }

  /**
   * Loops through the array of data and returns the title of the publication.
   *
   * @param array $data
   *   The data array from the JSON response.
   *
   * @return string
   *   The title of the publication.
   */
  private function getPublicationTitle(array $data) {
    $title = '';
    foreach ($data as $key => $value) {
      if (strpos($key, "title") !== FALSE && !empty($value) && !(strpos($key, "container") !== FALSE)) {
        $title = is_array($value) ? $value[0] : $value;
        break;
      }
    }
    return $title;
  }

  /**
   * If the link is a PDF, return the link. Otherwise, return NULL.
   *
   * @param object $data
   *   The data object returned from the API.
   *
   * @return string|null
   *   The pdf link if it exists.
   */
  private function getPdfLink($data) {
    if (isset($data->link)) {
      foreach ($data->link as $link) {
        if (strtolower(substr($link->URL, -3, 3)) == "pdf") {
          $pdf = $link->URL;
          break;
        }
      }
    }
    return $pdf ?? NULL;
  }

  /**
   * Retrieves an array of references to render.
   *
   * @param object $data
   *   The data returned from the API.
   *
   * @return array
   *   A render array of references.
   */
  private function getPublicationReferences($data) {
    $references = [];
    if (isset($data->reference)) {
      foreach ($data->reference as $reference) {
        $refData = [];
        if (isset($reference->DOI)) {
          $refData = $this->getReferenceData($reference->DOI);
        }
        else {
          $refArray = (array) $reference;
          $refData = [
            'title' => $this->getPublicationTitle($refArray),
            'author' => $refArray['author'] ?? '',
            'year' => $refArray['year'] ?? '',
            'doi' => $refArray['DOI'] ?? '',
          ];
        }
        $references[] = [
          '#theme' => 'doi_search_reference',
          '#items' => $refData,
        ];
      }
    }
    return $references;
  }

  /**
   * Formats an array of authors into a string.
   *
   * @param array $authors
   *   The authors of the article.
   *
   * @return string
   *   The formatted authors string.
   */
  private function formatAuthors(array $authors) {
    $formattedAuthors = [];
    foreach ($authors as $author) {
      $given = isset($author->given) ? $author->given . ' ' : '';
      if (empty($given) && !isset($author->family)) {
        return $author->name ?? '';
      }
      $formattedAuthors[] = $given . $author->family;
    }
    return implode(', ', $formattedAuthors);
  }

}
